from django import forms
from apps.mascota.models import Mascota

class mascotaform(forms.ModelForm):
    class Meta:
        model = Mascota
        fields = [
            'nombre',
            'sexo',
            'edad_aproximada',
            'color',
            'fecha_ingreso',
            'estado_Actual',
            'persona',
        ]
        labels = {
            'nombre':'Nombre',
            'sexo':'Sexo',
            'edad_aproximada':'Edad Aproximada',
            'color':'Color',
            'fecha_ingreso':'Fecha ingreso',
            'estado_Actual':'Estado Actual',
            'persona':'Adoptante',
        }
        widgest = {
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            'sexo':forms.TextInput(attrs={'class':'form-control'}),
            'edad_aproximada':forms.TextInput(attrs={'class':'form-control'}),
            'color':forms.TextInput(attrs={'class':'form-control'}),
            'fecha_ingreso':forms.TextInput(attrs={'class':'form-control'}),
            'estado_Actual':forms.Select(attrs={'class':'form-control'}),
            'persona':forms.TextInput(attrs={'class':'form-control'}),
        }