# README #

### Integrantes: ###
*Carlos Guitierrez
*Andrea Villacis
*Paola Guamani
*Graciela Moreno

### ¿Para qué quien se crea el sitio web?

El proyecto para el cual esta destinado este sitio web es POLI PERROS EPN
Que tiene como objetivos:
*Contribuir a mejorar las condiciones de vida de todos los animales.
*Velar por el respeto de los animales y la aplicación de sus derechos.
*Educar, formar e informar a la población sobre la tenencia responsable de un animal.
*Fomentar la adopción y concienciar a la población sobre la obligatoriedad de la identificación y los beneficios de la esterilización.
*Colaborar con otras asociaciones con ideas afines.
*Satisfacer las necesidades veterinarias de los animales que tenemos bajo nuestra protección y tutela.
*Buscar los adoptantes adecuados para cada tipo de animal y velar para que cumplan las cláusulas establecidas en el contrato de adopción de la asociación.
*Hacer el seguimiento de los animales adoptados.



### ¿Para qué sirve este repositorio? ###

El repositorio en Bitbucket contiene el codigo de la pagina web 

### ¿Cómo me configuro? ###

Para la creacion de ambiente vistual para nueswtro proyecto usamos 
*Django 1.9.6
*pysql2 2.6.1
*instalamos el pip

Al momento de ejecutar nuestra aplicacion,nos permite modificar visualizarla en la url:
https://localhost:8000

Para la creacion de la base se utiliza posgres


### Lineamientos de contribución ###

* Escritura de pruebas
* Revisión de código
*Incrementar las fucionalidades de acuerdo a lo mostrado en los boards
